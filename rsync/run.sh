#!/bin/bash
CONFIG_PATH=/data/options.json

KEEP_DAYS=$(jq --raw-output ".keep_days" $CONFIG_PATH)
RSYNC_ADDRESS=$(jq --raw-output ".server_address" $CONFIG_PATH)
RSYNC_DIR=$(jq --raw-output ".destination_directory" $CONFIG_PATH)
RSYNC_USER=$(jq --raw-output ".username" $CONFIG_PATH)
RSYNC_PASSWD=$(jq --raw-output ".password" $CONFIG_PATH)

echo "[Info] Starting remove and rsync"
echo "[Info] Removing backups older than $KEEP_DAYS days"
find /backup -mtime +$KEEP_DAYS -type f -delete

echo "[Info] Running rsync push to $RSYNC_ADDRESS into $RSYNC_DIR"

if [ -z "$RSYNC_USER" ]
then
echo "[Info] Pushing to rsync server without user"
rsync -av --delete /backup/ rsync://$RSYNC_ADDRESS/$RSYNC_DIR
else
echo "[Info] Pushing to rsync server with user: $RSYNC_USER"
sshpass -p $RSYNC_PASSWD rsync -av --delete /backup/ rsync://$RSYNC_USER@$RSYNC_ADDRESS/$RSYNC_DIR
fi
echo "[Info] Finished"
