# hassio-addons

## About

This repository contains hassos (formally hassio) add-ons.

See corresponding blog post for explanation of how to write an add-on https://jpselby.co.uk/projects/hassio-addon-rsync/.

## Installation

Follow the guide at https://www.home-assistant.io/hassio/installing_third_party_addons/ with the following URL:

`https://gitlab.com/jselby/hassio-addons`

## Configuration of add-ons

### Rsync

This addon will first remove the snapshots in the backup directory older than a specified numbers of days. Then will push the backup directory to a remote rsync server. The add-on supports both rsync servers that require a specified username/ password and those that don't.

### How to use

  1. Start the add-on
  2. The add-on will remove and rsync based on supplied configuration
  3. Check the add-on log output to confirm success

> **Note:** This add-on can be started by an automation (see service hassio.addon_start) 

### Configuration

Add-on configuration:

keep_days: 7  
server_address: ""  
destination_diriectory: ""  
username: ""  
password: ""  


**Option: keep_days (required)**

Backups older than this number of days will be deleted.

Default value: 7

**Option: server_address (required)**

The address of rsync server e.g. 192.168.1.100, rsync.domain.com.

**Option: destination_directory (required)**

The name of directory on rsync server to push to e.g. hassio_backups.

**Option: username (optional)**

The username used to authenticate with rsync server

**Option: password (optional)**

The password thats goes with the username configured for authentication
